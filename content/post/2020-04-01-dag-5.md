---
title: Dag 5 (Vecka 1)
date: 2020-04-01
---

Första skrivardagen. Jag satt och skrev bakgrunden för vårt projektarbete i PM 1. Gick igenom vad Arbetsförmedligen gör och vad de vill få ut av vårt projekt. Samt skrev ihop den kunskapen vi redan besitter och 
vad vi kommer att behöva lära oss.