---
title: Dag 7 (Vecka 1)
date: 2020-04-03
---

Fortsatte att försöka lösa mitt problem med Docker. Lyckades efter mycket om och men hitta att det var en regression i versionen jag använde. Bytte till en tidigare version och nu har vi fungerande CI/CD med Docker.
Började även med att göra om allt till microservices istället, samt började med tester för att Registrera användare.
